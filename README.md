# VuexORM Sugar

A plugin that provides some syntactic sugar on top or VuexORM.

## Implicit queries

Instead of explicitly getting a query for a model, you can directly call query 
methods on the model class:

    Post.whereId(7).get()
   
Instead of:

    Post.query().whereId(7).get()
   
## Iterable support on queries/models

    for (let post of Post.where(...))
    const [post] = Post.where(...)
   
 Instead of:
 
    for (let post of Post.query().where(...).get())
    const post = Post.query().where().first()

And even:

    const posts = [...Post]
   
## POJO support in where

    const sender = 'me'
    const sendTime = new Date()
    const myPosts = [...Post.where({ sender, sendTime })]
   
Instead of:

    const sender = 'me'
    const sendTime = new Date()
    const myPosts = Post.query().where('sender', sender).where('sendTime', sendTime).get()
   
## Scope support

    class Post extends VuexORM.Model {
      static fields () {
        return {
          id: this.attr(),
          sender: this.attr(),
          sendTime: this.attr(),

          next: this.belongsTo(Post, 'nextId')
        }
      }
    }

    Post.entity = 'posts'
    Post.scopes({
      forSender: (q, sender) =>  q.where({ sender }) },
      forTime: (q, sendTime) => q.where({ sendTime }) },
      forLast24Hours () { return this.where(r => r.sendTime > (Date.now() - 24 * 60 * 60 * 1000)) }  
    })

    const [post] = Post.forSender('me')
    const [post] = Post.forSender('me').forTime(new Date ())
    const [post] = Post.forSender('me').with('next', q => q.forSender('you'))
    const [post] = Post.forSender('me').with('next', Post.forLast24Hours)

## Changelog

### 0.0.2

* fixed issue with scopes that 'bled' into all other models

### 0.1.1

* backwards incompatible change: support for arrow function in scopes