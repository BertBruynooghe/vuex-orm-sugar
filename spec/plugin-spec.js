const Vue = require('vue')
const Vuex = require('vuex')
const VuexORM = require('@vuex-orm/core')
const  {plugin } = require('../lib/plugin')
const assert = require('assert')

Vue.use(Vuex)
VuexORM.use(plugin)

class Post extends VuexORM.Model {
  static fields () {
    return {
      id: this.attr(),
      content: this.attr(),
      nextId: this.attr(),

      next: this.belongsTo(Post, 'nextId')
    }
  }
}

Post.entity = 'posts'
Post.scopes({
  forId: (q, id) => q.whereId(id),
  forTwo () { return this.whereId(2) }
})

// test class to see if scopes are scoped to one model only
class PostExtension extends Post {}
PostExtension.scopes({ forId (id) { return undefined }})

describe('VuexORM Sugar plugin', () => {
  beforeEach(() => {
    const database = new VuexORM.Database()
    database.register(Post, {})

    new Vuex.Store({ // eslint-disable-line no-new
      plugins: [VuexORM.install(database)],
      strict: true
    })
  })

  describe('iterable support', () => {
    it('allows treating query as an iterable ', async () => {
      const { posts } = await Post.create({ data: [{ id: 1 }, { id: 2 }] })
      for(let { id } of Post.query()) {
        posts.splice(posts.findIndex(p => p.id === id), 1)
      }
      assert(posts.length === 0)
    })

    it('allows treating model as an iterable', async () => {
      const { posts } = await Post.create({ data: [{ id: 1 }, { id: 2 }] })
      for(let { id } of Post) {
        posts.splice(posts.findIndex(p => p.id === id), 1)
      }
      assert(posts.length === 0)
    })
  })

  describe('query methods on model', () => {
    it('allows treating model as query', async () => {
      await Post.create({ data: [{ id: 1 }, { id: 2 }] })

      assert(Post.whereId(1).first().id === 1)
    })
  })

  describe('scopes', () => {
    it('can be called', async () => {
      await Post.create({ data: [{ id: 1 }, { id: 2 }] })
      assert(Post.forId(1).first().id === 1)
    })
    
    it('can be called chained', async () => {
      await Post.create({ data: [{ id: 1 }, { id: 2 }] })
      assert(Post.forId(1).forId(1).first().id === 1)
    })

    it('can be used as methods', async () => {
      await Post.create({ data: [{ id: 1 }, { id: 2 }] })
      assert(Post.forTwo().first().id === 2)
    })

    it(`can be used on 'with' constraints`, async () => {
      await Post.create({ data: { id: 1, next: { id: 2 } } })
      const [post] = Post.forId(1).with('next', q => q.forId(2))
      assert(post.next.id === 2)
    })

    it(`can be used as 'with' constraint`, async () => {
      await Post.create({ data: { id: 1, next: { id: 2 } } })
      const [post] = Post.forId(1).with('next', Post.forTwo)
      assert(post.next.id === 2)
    })
  })

  describe('where extension', () => {
    it('supports POJOs', async () => {
      await Post.create({ data: [{ id: 1, content: 's' }, { id: 2 }] })
      assert(Post.where({ content: 't' }).first() === null)
      assert(Post.where({ content: 's' }).first().id === 1)
    })
  })
})
