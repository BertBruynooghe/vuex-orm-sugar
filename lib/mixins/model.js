module.exports = Model => {
    const queryWithoutScopes = Model.query
    return {
      [Symbol.iterator] () { return this.query()[Symbol.iterator]() },
      scopes (scopes) {
        const installedScopes = Object.keys(scopes).reduce(
          (acc, k) =>({ ...acc, [k]: (...args) =>  scopes[k].call(this, this, ...args) }), {})
        Object.assign(this, { installedScopes })
        Object.assign(this, installedScopes)
      },
      query (...args) {
        const q = queryWithoutScopes.apply(this, ...args)
        return Object.assign(q, q.model.installedScopes)
      }
    }
  }