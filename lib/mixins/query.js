module.exports = prototype => {
    const whereWithoutObject = prototype.where
    const withWithoutScopes = prototype.with
    return {
      [Symbol.iterator] () { return (this.get() || []).values() },
      where (obj, ...rest) {
        if (typeof obj !== 'object') return whereWithoutObject.call(this, obj, ...rest)
        return Object.keys(obj).reduce((q, k) => q.where(k, obj[k], ...rest), this)
      },
      with (name, constraint = null) {
        const newConstraint = q => {
          if (!constraint) return true
          const { model: { installedScopes = [] } } = q
          if (Object.values(installedScopes).includes(constraint))
            return constraint.call(q)
          for (scope in installedScopes) q[scope] = installedScopes[scope]
          return constraint(q) 
        }
        return withWithoutScopes.call(this, name, newConstraint)
      }
    }
  }