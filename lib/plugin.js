const queryMixin = require('./mixins/query')
const modelMixin = require('./mixins/model') 

module.exports.plugin = {
  install ({ Query, Model }, options) {
    Object.assign(Query.prototype, queryMixin(Query.prototype))
    Object.assign(Model, modelMixin(Model))

    for (let method in Query.prototype) {
      if (Model[method]) continue
      Model[method] = function (...args) { return this.query()[method](...args) }
    }
  }
}
